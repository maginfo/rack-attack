
bots list is taken from https://github.com/opawg/user-agents

## Rails app

* no database
* skipped frontend portion, but if needed I can write Vue/Nuxt app for that
* user-agents and iP lists (safe and block) are in static folder and appropriate modules in app/lib folder
* goal was to have rack-attack.rb as clean as possible and to extend request method so method calls are also simple (request.ip_safelisted?, request.user_agent_blocklisted?)
* Fail2Ban wasn't needed as we don't block anybody based on hits (in this app), so throttling was enough
* data needed for Rack::Attack statistics is would go to instrumentation.rb file (if needed)
* SlackNotifier is set to send notification to some channel on blocked requests
* String class is not important, just wanted to use it to emphasize blocked vs throttled requests


## Test app - main.rb

App can be tested with ruby file and --help will list all options

```
ruby main.rb --help
```

#### Examples:

1. Start Rails app (rails s)
2. Url is localhost:3000
3. In second terminal run test app: `ruby main.rb --help` to display switches

#### Switches and expected behaviour 

| ruby main.rb + switches | behaviour |
|--------------------------|--------------------|
| -i 1.1.1.1 | Will hit localhost:3000 once with IP 1.1.1.1 and be blocked |
| -a "DuckDuckBot/1.0; (+http://duckduckgo.com/duckduckbot.html)" -n 100 | localhost:3000, 100 times, all allowed, based on UA safelist |
| -a "Podcasts/1410.53 CFNetwork/1111 Darwin/19.0.0 (x86_64)" -n 100 | localhost:3000, 100 times, all allowed, based on UA safelist |
| -n 10 | First 5 requests will be allowed, then throttled |
| -n 10 -a Vacuum | All blocked, based on blocklist |
| -n 1000 -r | Will hit endpoint 1000 times using randomized data (IP, User Agent) from test.yaml file  |
||


## RSPEC

App is tested and goal was to reach 100% coverage