# frozen_string_literal: true
require 'request' # defines helpful monkeypatches used here
require 'string'

class Rack::Attack

  cache.store = ActiveSupport::Cache::RedisCacheStore.new(
    host: ENV.fetch('REDIS_HOST', 'localhost'), port: ENV.fetch('REDIS_PORT', 6379)
  )
  cache.prefix = 'rack-attack'

  # # Load dynamic rules
  # bots = Rules::Bots.new(Rails.root.join('static', 'rack-attack-rules.yaml'))
  # lists = Rules::Lists.new(Rails.root.join('static', 'rack-attack-rules.yaml'))

  # Allow all defined safelisted ips
  safelist('allow-admin-defined-set-of-ips') do |req|
    req.ip_safelisted?
  end

  # Allow all defined agents/bots
  safelist('safe-user-agents') do |req|
    # bots.safe?(req.user_agent)
    req.user_agent_safelisted?
  end

  # block already blacklisted
  blocklist('block blacklisted IP') do |req|
    req.ip_blocklisted?
  end

  # block unsafe bots
  blocklist('block bad user agents') do |req|
    # bots.unsafe?(req.user_agent)
    req.user_agent_blocklisted?
  end

  # Allow an "ordinary" user IP address to make 50 requests/day
  # for testing, I used 5 hits in 60 seconds

  throttle('req/ip', limit: 5, period: 1.day, &:ip)

  # Send the following response to throttled clients
  # self.throttled_response = ->(env) {
  #   retry_after = (env['rack.attack.match_data'] || {})[:period]
  #   [
  #     429,
  #     { 'Content-Type' => 'application/json', 'Retry-After' => retry_after.to_s },
  #     [{ error: 'Throttle limit reached. Retry later.' }.to_json]
  #   ]
  # }
end
