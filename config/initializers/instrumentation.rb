# frozen_string_literal: true

ActiveSupport::Notifications.subscribe(/rack_attack/) do |name, start, finish, request_id, payload|

  req = payload[:request]

  case req.env['rack.attack.match_type']
  when :throttle
    msg = [[Rack::Attack], req.env['rack.attack.match_type'], "IP: #{req.ip}", "method: #{req.request_method}", "path: #{req.fullpath}", "agent: #{req.user_agent}"].join('; ')
    Rails.logger.debug msg.blue
  when :blocklist
    msg = [[Rack::Attack], req.env['rack.attack.match_type'], "IP: #{req.ip}", "method: #{req.request_method}", "path: #{req.fullpath}", "agent: #{req.user_agent}"].join('; ')
    Rails.logger.debug msg.red
    SlackNotifierService.new(name, start, finish, request_id, req.ip, req.path).notify

  end
end
