require 'rails_helper'

describe Rack::Attack, type: :request do
  include Rack::Test::Methods

  let(:app) { Rails.application }

  before do
    header 'USER_AGENT', user_agent
    header 'X-Forwarded-For', ip
  end

  after :each do
    Rack::Attack.reset!
  end

  describe 'throttle excessive requests by IP address' do
    let(:ip) { '10.10.10.10' }
    let(:user_agent) { 'rspec-agent' }
    let(:limit) { 5 }

    context 'number of requests is lower than the limit' do
      it 'does not change the response status' do
        limit.times do
          get '/'
          expect(last_response.status).to eq(200)
        end
      end
    end

    context 'number of requests is higher than the allowed limit' do
      let(:ip) { '10.10.10.10' }
      let(:user_agent) { 'rspec-agent' }
      it 'throttle excessive requests by IP address' do
        limit = 5
        period = 1.day

        limit.times do
          Rack::Attack.cache.count("req/ip:#{ip}", period)
        end

        get '/'

        # next request
        expect(last_response.status).to eq(429)
      end

      it 'changes the response status to 429' do
        (limit * 2).times do |i|
          get '/'
          expect(last_response.status).to eq(429) if i > limit
        end
      end
    end
  end

  describe 'block requests by IP address or user agent if it is' do
    context 'blocklisted' do
      context 'IP address' do
        let(:ip) { '1.1.1.1' }
        let(:user_agent) { 'rspec-agent' }

        it 'cannot make any requests' do
          get '/'

          expect(last_response.status).to eq(403)
        end
      end

      context 'user agent' do
        let(:ip) { '10.10.10.10' }
        let(:user_agent) { 'Vacuum' }

        it 'cannot make any requests' do
          get '/'

          expect(last_response.status).to eq(403)
        end
      end
    end
  end
end
