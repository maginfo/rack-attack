require 'rails_helper'

describe 'Extending the request object', type: :request do
  include Rack::Test::Methods

  let(:request) {
    Rack::Attack::Request.new(hash)
  }

  describe '#ip_safelisted?' do
    context 'address is in safe list' do
      let(:hash) { { 'REMOTE_ADDR' => '1.2.3.4' } }

      it 'and it is safelisted' do
        expect(request.ip_safelisted?).to eq(true)
      end
    end

    context 'address is not on the safe list' do
      let(:hash) { { 'REMOTE_ADDR' => '2.2.2.2' } }

      it 'and it is not safelisted' do
        expect(request.ip_safelisted?).to eq(false)
      end
    end
  end

  describe '#ip_blocklisted?' do
    context 'address is in block list' do
      let(:hash) { { 'REMOTE_ADDR' => '1.1.1.1' } }

      it 'and it is blocklisted' do
        expect(request.ip_blocklisted?).to eq(true)
      end
    end

    context 'address is not on the block list' do
      let(:hash) { { 'REMOTE_ADDR' => '2.2.2.2' } }

      it 'and it is not blocklisted' do
        expect(request.ip_blocklisted?).to eq(false)
      end
    end
  end

  describe '#user_agent_safelisted?' do
    context 'useragent is on the safe list' do
      let(:hash) { { 'HTTP_USER_AGENT' => 'de.danoeh.antennapod/1.7.3b (Linux;Android 8.0.0) ExoPlayerLib/2.9.3' } }

      it 'and it is safelisted' do
        expect(request.user_agent_safelisted?).to eq(true)
      end
    end

    context 'address is not on the safe list' do
      let(:hash) { { 'HTTP_USER_AGENT' => 'rspec-agent' } }

      it 'and it is not safelisted' do
        expect(request.user_agent_safelisted?).to eq(false)
      end
    end
  end

  describe '#user_agent_blocklisted?' do
    context 'useragent is on the block list' do
      let(:hash) { { 'HTTP_USER_AGENT' => 'LinkWalker' } }

      it 'and it is blocklisted' do
        expect(request.user_agent_blocklisted?).to eq(true)
      end
    end

    context 'useragent is not on the block list' do
      let(:hash) { { 'HTTP_USER_AGENT' => 'rspec-agent' } }

      it 'and it is not blocklisted' do
        expect(request.user_agent_blocklisted?).to eq(false)
      end
    end
  end
end
