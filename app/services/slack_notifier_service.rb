# frozen_string_literal: true

require 'net/http'

class SlackNotifierService
  # include Sidekiq::Worker

  DATA = {
    username: 'blocked_requests bot',
    icon_emoji: ':ghost:',
  }

  def initialize(name, start, finish, request_id, remote_ip, path)
    @name = name
    @start = start
    @finish = finish
    @request_id = request_id
    @remote_ip = remote_ip
    @path = path
  end

  def notify
    # generate payload
    generate_payload

    # deliver notification
    deliver
  end

  private

  def generate_payload
    @payload = "[Rack::Attack][Blocked] remote_ip: #{@remote_ip}"
  end

  def deliver
    # slack webhook
    DATA[:text] = @payload

    @uri = URI(ENV.fetch('SLACK_WEBHOOK_URL', ''))
    @params = { payload: DATA.to_json }

    Net::HTTP.post_form(@uri, @params)

  rescue StandardError => e
    Rails.logger.error("SlackNotifier: Error when sending: #{e.message}")
  end
end
