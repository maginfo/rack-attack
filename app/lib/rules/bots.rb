# frozen_string_literal: true

module Rules
  class Bots < Base

    SAFE_MATCHERS = 'safe_bots'
    UNSAFE_MATCHERS = 'unsafe_bots'

    private

    def detect_safe_request(user_agent)
      return false if user_agent.blank?

      result = matchers_list(SAFE_MATCHERS).detect { |bot| bot['user_agents'].detect { |item| user_agent.match? item } }

      result.present?
    end

    def detect_unsafe_request(user_agent)
      return true if user_agent.blank?

      result = matchers_list(UNSAFE_MATCHERS).detect { |bot| user_agent.match? bot }

      result.present?
    end
  end
end
