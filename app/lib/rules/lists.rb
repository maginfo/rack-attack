# frozen_string_literal: true

module Rules
  class Lists < Base

    SAFE_MATCHERS = 'safelisted'
    UNSAFE_MATCHERS = 'blocklisted'

    private

    def detect_safe_request(matcher)
      matchers_list(SAFE_MATCHERS).include?(matcher)
    end

    def detect_unsafe_request(matcher)
      matchers_list(UNSAFE_MATCHERS).include?(matcher)
    end
  end
end
