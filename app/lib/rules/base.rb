# frozen_string_literal: true

module Rules
  class Base

    def initialize(path)
      @path = path
    end

    def safe?(matcher)
      detect_safe_request(matcher)
    end

    def unsafe?(matcher)
      detect_unsafe_request(matcher)
    end

    private

    def matchers_list(list_name)
      load_yaml[list_name]
    end

    def load_yaml
      @load_yaml ||= YAML.load_file(@path)
    end
  end
end
