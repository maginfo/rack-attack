class Rack::Attack::Request < ::Rack::Request

  def initialize(env)

    # Load dynamic rules
    @bots = Rules::Bots.new(Rails.root.join('static', 'rack-attack-rules.yaml'))
    @lists = Rules::Lists.new(Rails.root.join('static', 'rack-attack-rules.yaml'))

    super
  end

  def ip_safelisted?
    @lists.safe?(ip)
  end

  def ip_blocklisted?
    @lists.unsafe?(ip)
  end

  def user_agent_safelisted?
    @bots.safe?(user_agent)
  end

  def user_agent_blocklisted?
    @bots.unsafe?(user_agent)
  end
end
