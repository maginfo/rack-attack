#!/usr/bin/ruby
require 'json'
require 'net/http'
require 'open-uri'
require 'optparse'
require 'uri'
require 'yaml'

def randomize_ip
  ip = yaml_data['ip'].sample
  IPAddr.new(ip).to_range.to_a.sample.to_s
end

def randomize_agent
  yaml_data['agents'].sample
end

def yaml_data
  @yaml_data ||= YAML.load_file('test.yaml')
end

##########################################################################

options = {}

optparse = OptionParser.new do|opts|
  opts.banner = 'Usage: test.rb [options] ...'

  options[:verbose] = false
  opts.on('-v', '--verbose', 'Output more information') do
    options[:verbose] = true
  end

  options[:url] = 'http://localhost:3000'
  opts.on('-u', '--url [URL]', 'Endpoint (default is localhost:3000)') do |value|
    options[:url] = value
  end

  opts.on('-d', '--delay [DELAY]', 'Integer', 'Delay your requests (in seconds)') do |value|
    puts value
    options[:delay] = value
  end

  options[:number] = 1
  opts.on('-n', '--number [NUMBER]', 'Integer', 'Number of hits (0 unlimited)') do |value|
    options[:number] = value
  end

  options[:agent] = 'User-Agent'
  opts.on('-a', '--agent [AGENT]', 'User-Agent') do |value|
    options[:agent] = value
  end

  options[:randomize] = false
  opts.on('-r', '--randomize', 'Use random IP and User-Agent from test.yaml file') do
    options[:randomize] = true
  end

  options[:ip] = '10.10.10.10'
  opts.on('-i', '--ip [IP]', 'IP Address you want to use') do |value|
    options[:ip] = value
  end

  options[:path] = '/'
  opts.on('-p', '--path [PATH]', 'IP Address you want to use') do |value|
    options[:path] = value
  end

  opts.on('-h', '--help', 'Display this screen') do
    puts opts
    exit
  end
end

optparse.parse!

puts options

# only get requests
options[:number].to_i.times.each do
  uri = URI(options[:url])

  options[:ip] = randomize_ip if options[:randomize]
  options[:agent] = randomize_agent if options[:randomize]

  
  unless uri.host || uri.port
    puts 'Wrong url'
    exit
  end
  
  http = Net::HTTP.new uri.host, uri.port
  
  headers = {}
  headers['User-Agent'] = options[:agent] if options[:agent]
  headers['X-Forwarded-For'] = options[:ip] if options[:ip]

  puts 'New request'
  puts "Opening #{uri.host}:#{uri.port} with ip: #{options[:ip]}, agent: #{options[:agent]}"

  res = http.get options[:path], headers

  puts "Opened #{options[:ip]}#{options[:path]} | Response: #{res.code} - #{res.message}"
  sleep options[:delay].to_i/1000 if options[:delay]

end


